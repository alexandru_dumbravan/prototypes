package com.example.alex.resultreceiverexample.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alex on 11/2/2015.
 */
public class ServiceOutput implements Parcelable {

    private String message;

    public ServiceOutput() {

    }

    public ServiceOutput(Parcel source) {
        message = source.readString();
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
    }

    public static final Creator<ServiceOutput> CREATOR = new Creator<ServiceOutput>() {
        @Override
        public ServiceOutput createFromParcel(Parcel source) {
            return new ServiceOutput(source);
        }

        @Override
        public ServiceOutput[] newArray(int size) {
            return new ServiceOutput[size];
        }
    };
}
