package com.example.alex.resultreceiverexample.service;

/**
 * Created by alex on 11/1/2015.
 */
public class ServiceUtils {

    public static final int RESULT_OK = 1;
    public static final int RESULT_ERROR = -1;

    public static final String RESULT_INFO = "resultInfo";
}
