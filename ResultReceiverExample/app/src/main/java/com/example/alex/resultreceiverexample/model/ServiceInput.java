package com.example.alex.resultreceiverexample.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alex on 11/1/2015.
 */
public class ServiceInput implements Parcelable {

    private boolean shouldMockSuccess;

    public ServiceInput() {

    }

    protected ServiceInput(Parcel source) {
        boolean[] values = new boolean[1];
        source.readBooleanArray(values);
        shouldMockSuccess = values[0];
    }

    public boolean shouldMockSuccess() {
        return shouldMockSuccess;
    }

    public void setShouldMockSuccess(boolean shouldMockSuccess) {
        this.shouldMockSuccess = shouldMockSuccess;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        boolean[] values = new boolean[1];
        values[0] = shouldMockSuccess;
        dest.writeBooleanArray(values);
    }

    public static final Creator<ServiceInput> CREATOR = new Creator<ServiceInput>() {
        @Override
        public ServiceInput createFromParcel(Parcel source) {
            return new ServiceInput(source);
        }

        @Override
        public ServiceInput[] newArray(int size) {
            return new ServiceInput[size];
        }
    };
}
