package com.example.alex.resultreceiverexample.activities;

import android.os.Handler;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.alex.resultreceiverexample.R;
import com.example.alex.resultreceiverexample.fragments.MainFragment;
import com.example.alex.resultreceiverexample.model.ServiceInput;
import com.example.alex.resultreceiverexample.model.ServiceOutput;
import com.example.alex.resultreceiverexample.receivers.AbstractServiceResultReceiver;
import com.example.alex.resultreceiverexample.service.MockService;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.button_error).setOnClickListener(this);
        findViewById(R.id.button_success).setOnClickListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, MainFragment.newInstance()).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_error:
                startService(false);
                break;
            case R.id.button_success:
                startService(true);
                break;
        }
    }

    private void startService(boolean mockSuccess) {
        ServiceInput serviceInput = new ServiceInput();
        serviceInput.setShouldMockSuccess(mockSuccess);
        startService(MockService.buildServiceIntent(getApplicationContext(), serviceInput, new ActivityResultReceiver(handler, this)));
    }


    private static class ActivityResultReceiver extends AbstractServiceResultReceiver<MainActivity> {

        public ActivityResultReceiver(Handler handler, MainActivity mainActivity) {
            super(handler, mainActivity);
        }

        @Override
        protected void onResultSuccess(MainActivity mainActivity, Parcelable info) {
            super.onResultSuccess(mainActivity, info);
            ServiceOutput serviceOutput = (ServiceOutput) info;
            Toast.makeText(mainActivity.getApplicationContext(), "Received success in activity. " + serviceOutput.getMessage(), Toast.LENGTH_LONG).show();
        }


        @Override
        protected void onResultError(MainActivity mainActivity, com.example.alex.resultreceiverexample.model.Error error) {
            super.onResultError(mainActivity, error);
            Toast.makeText(mainActivity.getApplicationContext(), "Received error in activity. Error code: " + error.getErrorCode(), Toast.LENGTH_LONG).show();
        }

        @Override
        protected boolean isValidReference(MainActivity reference) {
            return reference != null && !reference.isFinishing();
        }
    }
}
