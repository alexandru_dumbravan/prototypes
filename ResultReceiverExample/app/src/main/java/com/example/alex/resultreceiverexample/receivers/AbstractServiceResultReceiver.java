package com.example.alex.resultreceiverexample.receivers;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.ResultReceiver;

import com.example.alex.resultreceiverexample.model.*;
import com.example.alex.resultreceiverexample.service.ServiceUtils;

import java.lang.ref.WeakReference;

/**
 * Created by alex on 11/1/2015.
 */
public abstract class AbstractServiceResultReceiver<T> extends ResultReceiver {

    private boolean isRunning;
    private boolean isSuccess;
    private WeakReference<T> reference;

    public AbstractServiceResultReceiver(Handler handler, T t) {
        super(handler);
        reference = new WeakReference<T>(t);
        isRunning = true;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        T ref = reference.get();
        isRunning = false;
        if (ref != null && isValidReference(ref)) {
            switch (resultCode) {
                case ServiceUtils.RESULT_OK:
                    isSuccess = true;
                    onResultSuccess(ref, resultData.getParcelable(ServiceUtils.RESULT_INFO));
                    break;
                case ServiceUtils.RESULT_ERROR:
                    onResultError(ref, (com.example.alex.resultreceiverexample.model.Error) resultData.getParcelable(ServiceUtils.RESULT_INFO));
                    break;
            }
        }
    }

    protected abstract boolean isValidReference(T reference);

    protected void onResultSuccess(T t, Parcelable info) {

    }

    protected void onResultError(T t, com.example.alex.resultreceiverexample.model.Error error) {

    }


    public boolean isRunning() {
        return isRunning;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

}
