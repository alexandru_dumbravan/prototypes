package com.example.alex.resultreceiverexample.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alex on 11/1/2015.
 */
public class Error implements Parcelable {

    private int errorCode;

    public Error() {

    }

    protected Error(Parcel source) {
        errorCode = source.readInt();
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(errorCode);
    }

    public static final Creator<Error> CREATOR = new Creator<Error>() {
        @Override
        public Error createFromParcel(Parcel source) {
            return new Error(source);
        }

        @Override
        public Error[] newArray(int size) {
            return new Error[size];
        }
    };
}
