package com.example.alex.resultreceiverexample.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.alex.resultreceiverexample.R;
import com.example.alex.resultreceiverexample.model.ServiceInput;
import com.example.alex.resultreceiverexample.model.ServiceOutput;
import com.example.alex.resultreceiverexample.receivers.AbstractServiceResultReceiver;
import com.example.alex.resultreceiverexample.service.MockService;

/**
 * Created by alex on 11/2/2015.
 */
public class MainFragment extends Fragment implements View.OnClickListener {

    private Handler handler = new Handler();

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        root.findViewById(R.id.button_fragment_test).setOnClickListener(this);
        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_fragment_test:
                ServiceInput serviceInput = new ServiceInput();
                serviceInput.setShouldMockSuccess(true);
                getActivity().startService(MockService.buildServiceIntent(getActivity().getApplicationContext(), serviceInput, new FragmentResultReceiver(handler, this)));
                break;
        }
    }

    private static class FragmentResultReceiver extends AbstractServiceResultReceiver<MainFragment> {

        public FragmentResultReceiver(Handler handler, MainFragment mainFragment) {
            super(handler, mainFragment);
        }

        @Override
        protected void onResultSuccess(MainFragment mainFragment, Parcelable info) {
            super.onResultSuccess(mainFragment, info);
            ServiceOutput serviceOutput = (ServiceOutput) info;
            Toast.makeText(mainFragment.getActivity().getApplicationContext(), "Received success in fragment. " + serviceOutput.getMessage(), Toast.LENGTH_LONG).show();
        }


        @Override
        protected void onResultError(MainFragment mainFragment, com.example.alex.resultreceiverexample.model.Error error) {
            super.onResultError(mainFragment, error);
            Toast.makeText(mainFragment.getActivity().getApplicationContext(), "Received error in fragment. Error code: " + error.getErrorCode(), Toast.LENGTH_LONG).show();
        }

        @Override
        protected boolean isValidReference(MainFragment mainFragment) {
            return mainFragment != null && mainFragment.isAdded();
        }
    }
}
