package com.example.alex.resultreceiverexample.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.example.alex.resultreceiverexample.model.*;
import com.example.alex.resultreceiverexample.model.Error;

/**
 * Created by alex on 11/2/2015.
 */
public class MockService extends IntentService {


    private static final String EXTRA_SERVICE_INPUT = "extraServiceInput";
    private static final String EXTRA_RESULT_RECEIVER = "extraResultReceiver";

    public static Intent buildServiceIntent(Context context, ServiceInput serviceInput, ResultReceiver resultReceiver) {
        Intent intent = new Intent(context.getApplicationContext(), MockService.class);
        intent.putExtra(EXTRA_SERVICE_INPUT, serviceInput);
        intent.putExtra(EXTRA_RESULT_RECEIVER, resultReceiver);
        return intent;
    }

    public MockService() {
        super(MockService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ServiceInput serviceInput = intent.getParcelableExtra(EXTRA_SERVICE_INPUT);
        ResultReceiver resultReceiver = intent.getParcelableExtra(EXTRA_RESULT_RECEIVER);
        Bundle result = new Bundle();
        if (serviceInput.shouldMockSuccess()) {
            ServiceOutput serviceOutput = new ServiceOutput();
            serviceOutput.setMessage("This was a success");
            result.putParcelable(ServiceUtils.RESULT_INFO, serviceOutput);
            resultReceiver.send(ServiceUtils.RESULT_OK, result);
        } else {
            com.example.alex.resultreceiverexample.model.Error error = new Error();
            error.setErrorCode(100);
            result.putParcelable(ServiceUtils.RESULT_INFO, error);
            resultReceiver.send(ServiceUtils.RESULT_ERROR, result);
        }
    }
}
