package com.example.alex.generic_loader.asyncload;

/**
 * Interface definition for a set of callbacks in different stages of the loading process
 *
 * @param <I> Defines an additional input for data loading when the key does not suffice
 * @param <K> Defines the key used to cache data
 * @param <V> Defines the type of data to be loaded and cached
 *            Created by alex on 6/8/2015.
 */
public interface Viewable<I, K extends Comparable<K>, V> {

    /**
     * Called when the data finished loading.
     *
     * @param value the value that was retrieved when loading
     */
    void update(V value);

    /**
     * Called when the loading process reaches different states: loading, canceling.
     *
     * @return the loadable that indicates how the data should be loaded
     */
    Loadable<I, K, V> getLoadable();

    /**
     * Called after the async task was created but before the loading started.
     *
     * @param task
     */
    void createModelable(WorkerTask<I, K, V> task);

    /**
     * Called in various steps of the loading process to check if it can cancel an unrelated work or to check if the current task is the same as the one that is associated with the data
     *
     * @return a reference to the object holding the WorkerTask reference from #createModelable method
     */
    Modelable<I, K, V> getModelable();

}
