package com.example.alex.generic_loader.asyncload;

import android.os.AsyncTask;
import android.util.LruCache;

/**
 * This class is responsible for storing the data in the cache, making sure that the data is not set in a different area
 *
 * @param <I> Defines an additional input for data loading when the key does not suffice
 * @param <K> Defines the key used to cache data
 * @param <V> Defines the type of data to be loaded and cached
 *            Created by alex on 6/8/2015.
 */
public class LoadManager<I, K extends Comparable<K>, V> implements WorkerTask.OnLoadListener<K, V> {


    private LruCache<K, V> memoryCache;

    /**
     * Creates a new loader manager with the cache size of 10 items
     */
    public LoadManager() {
        memoryCache = new LruCache<K, V>(10);
    }

    /**
     * This method uses the default thread pool for async tasks to do background work
     *
     * @param viewable the viewable on which the data will be set
     * @param key      the key to store the view in the cache
     * @param input    additional input for loading the data
     */
    public void loadDataOnDefaultExecutor(Viewable<I, K, V> viewable, K key, I input) {
        loadData(viewable, key, input, true);
    }

    /**
     * This method uses #AsyncTask.THREAD_POOL_EXECUTOR as the threadpool do to background work
     *
     * @param viewable the viewable on which the data will be set
     * @param key      the key to store the view in the cache
     * @param input    additional input for loading the data
     */
    public void loadDataOnParallelExecutor(Viewable<I, K, V> viewable, K key, I input) {
        loadData(viewable, key, input, false);
    }

    private void loadData(Viewable<I, K, V> viewable, K key, I input, boolean shouldUseDefaultExecutor) {
        final V cacheValue = getFromMemCache(key);
        if (cacheValue != null) {
            viewable.update(cacheValue);
        } else {
            if (cancelPotentialWork(viewable, key)) {
                final WorkerTask<I, K, V> task = new WorkerTask<I, K, V>(viewable, this, key, input);
                viewable.createModelable(task);
                if (!shouldUseDefaultExecutor) {
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    task.execute();
                }

            }
        }
    }

    private synchronized void addToMemoryCache(K key, V cacheValue) {
        if (getFromMemCache(key) == null && cacheValue != null) {
            memoryCache.put(key, cacheValue);
        }
    }

    private synchronized V getFromMemCache(K key) {
        return memoryCache.get(key);
    }

    private boolean cancelPotentialWork(Viewable<I, K, V> viewable, K key) {
        if (viewable != null) {
            Modelable<I, K, V> modelable = viewable.getModelable();
            if (modelable != null) {
                final WorkerTask<I, K, V> workerTask = modelable.getWorkerTask();
                if (workerTask != null) {
                    final K pendingKey = workerTask.getCacheKey();
                    if (pendingKey == null || pendingKey.compareTo(key) != 0) {
                        workerTask.cancel(true);
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public void onLoadFinished(K key, V value) {
        addToMemoryCache(key, value);
    }
}
