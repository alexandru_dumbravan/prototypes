package com.example.alex.generic_loader.asyncload;

/**
 * Created by alex on 6/11/2015.
 */
public class ImageLoadInput {

    private int width;
    private int height;

    public ImageLoadInput(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
