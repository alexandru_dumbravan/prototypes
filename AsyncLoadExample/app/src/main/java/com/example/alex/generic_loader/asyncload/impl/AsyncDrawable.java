package com.example.alex.generic_loader.asyncload.impl;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import com.example.alex.generic_loader.asyncload.ImageLoadInput;
import com.example.alex.generic_loader.asyncload.Modelable;
import com.example.alex.generic_loader.asyncload.WorkerTask;

import java.lang.ref.WeakReference;

/**
 * Created by alex on 6/15/2015.
 */
public class AsyncDrawable extends BitmapDrawable implements Modelable<ImageLoadInput, Integer, Bitmap> {

    private WeakReference<WorkerTask<ImageLoadInput, Integer, Bitmap>> workerTask;

    public AsyncDrawable(Resources res, Bitmap bitmap, WorkerTask<ImageLoadInput, Integer, Bitmap> task) {
        super(res, bitmap);
        this.workerTask = new WeakReference<WorkerTask<ImageLoadInput, Integer, Bitmap>>(task);
    }

    @Override
    public WorkerTask<ImageLoadInput, Integer, Bitmap> getWorkerTask() {
        return workerTask.get();
    }


}
