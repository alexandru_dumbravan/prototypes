package com.example.alex.generic_loader.asyncload;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

/**
 * Class responsible for loading the data async and setting it on the appropriate viewable
 *
 * @param <I> Defines an additional input for data loading when the key does not suffice
 * @param <K> Defines the key used to cache data
 * @param <V> Defines the type of data to be loaded and cached
 *            Created by alex on 6/8/2015.
 */
public class WorkerTask<I, K extends Comparable<K>, V> extends AsyncTask<Void, Void, V> {
    private final WeakReference<Viewable<I, K, V>> updatableWeakReference;
    private K cacheKey;
    private I input;
    private OnLoadListener<K, V> onLoadListener;

    public WorkerTask(Viewable<I, K, V> viewable, OnLoadListener<K, V> listener, K key, I input) {
        this.updatableWeakReference = new WeakReference<Viewable<I, K, V>>(viewable);
        this.onLoadListener = listener;
        this.cacheKey = key;
        this.input = input;
    }

    @Override
    protected V doInBackground(Void... params) {
        final Viewable<I, K, V> viewable = updatableWeakReference.get();
        if (viewable != null) {
            Loadable<I, K, V> loadable = viewable.getLoadable();
            V result = loadable.loadData(this.input, this.cacheKey);
            onLoadListener.onLoadFinished(cacheKey, result);
            return result;
        }
        return null;
    }

    @Override
    protected void onPostExecute(V value) {
        if (isCancelled()) {
            value = null;
        }

        if (value != null) {
            final Viewable<I, K, V> viewable = updatableWeakReference.get();
            if (viewable != null) {
                Modelable<I, K, V> modelable = viewable.getModelable();
                if (modelable != null) {
                    final WorkerTask<I, K, V> workerTask = modelable.getWorkerTask();
                    if (this == workerTask) {
                        viewable.update(value);
                    }
                }
            }
        }
    }

    public K getCacheKey() {
        return cacheKey;
    }


    /**
     * Interface definition for a callback that indicates when the load is finished
     *
     * @param <K> the cache key of the data
     * @param <V> the cache value of the data
     */
    public interface OnLoadListener<K, V> {

        /**
         * Called when a load finishes
         *
         * @param key   the key used to load the data
         * @param value the loaded value
         */
        void onLoadFinished(K key, V value);
    }
}
