package com.example.alex.generic_loader.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.alex.generic_loader.R;
import com.example.alex.generic_loader.adapters.ListAdapter;
import com.example.alex.generic_loader.bean.ListItem;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<ListItem> items = new ArrayList<ListItem>();


        items.add(new ListItem(R.drawable.simley_10_256, "The images are actually part of the text set in the text view :-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.smiley_10_64, ":-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O :-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.smiley_9_48, ":-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O :-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O :-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.smiley_6_64, "test1 :-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.simley_12_24, "test2 :-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.smiley_8_256, "test3 :-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.simley_10_256, ":-) some text, :-( , ;-) , :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.simley_10_256, ":-) , :-( , some text ;-) , :-P , =-O ,  :-* ,  :O"));

        items.add(new ListItem(R.drawable.simley_10_256, ":-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.simley_10_256, ":-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O                            :-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.simley_10_256, ":-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O     :-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.smiley_10_64, ":-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O :-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O      "));
        items.add(new ListItem(R.drawable.smiley_9_48, ":-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O \n :-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.smiley_6_64, "test1"));
        items.add(new ListItem(R.drawable.simley_12_24, "test2"));
        items.add(new ListItem(R.drawable.smiley_8_256, "test3"));
        items.add(new ListItem(R.drawable.smiley_6_32, ":-) , :-( , ;-) , some text :-P , =-O ,  :-* ,  :O"));
        items.add(new ListItem(R.drawable.smiley_6_32, ":-) , :-( , ;-) , :-P , =-O ,  :-* ,  :O"));


        ListAdapter adapter = new ListAdapter(this, items);
        ListView listView = (ListView) findViewById(R.id.list);
        listView.addHeaderView(LayoutInflater.from(this).inflate(R.layout.list_header, null));
        listView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
