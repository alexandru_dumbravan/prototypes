package com.example.alex.generic_loader.asyncload;

/**
 * Interface definition for a set of callbacks related to asynchronous loading
 *
 * @param <I> Defines an additional input for data loading when the key does not suffice
 * @param <K> Defines the key used to cache data
 * @param <V> Defines the type of data to be loaded and cached
 *            Created by alex on 6/8/2015.
 */
public interface Loadable<I, K extends Comparable<K>, V> {


    /**
     * Called on a separate thread. Here is where the data should be loaded for display
     *
     * @param input additional input for the data along the key
     * @param key   the cache key which with which the data is stored
     * @return the loaded and ready to be cached data
     */
    V loadData(I input, K key);
}
