package com.example.alex.generic_loader.asyncload.impl;

import android.content.res.Resources;
import android.text.Spannable;
import android.text.SpannableString;

import com.example.alex.generic_loader.asyncload.Modelable;
import com.example.alex.generic_loader.asyncload.WorkerTask;

import java.lang.ref.WeakReference;

/**
 * Created by alex on 6/15/2015.
 */
public class EmojiSpannable extends SpannableString implements Modelable<String, String, Spannable> {



    private WeakReference<WorkerTask<String, String, Spannable>> workerTask;

    public EmojiSpannable(CharSequence source, Resources res, WorkerTask<String, String, Spannable> task) {
        super(source);
        this.workerTask = new WeakReference<WorkerTask<String, String, Spannable>>(task);
    }

    @Override
    public WorkerTask<String, String, Spannable> getWorkerTask() {
        return workerTask.get();
    }


}
