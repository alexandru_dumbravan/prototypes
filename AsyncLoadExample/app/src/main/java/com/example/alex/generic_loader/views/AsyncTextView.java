package com.example.alex.generic_loader.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.alex.generic_loader.R;
import com.example.alex.generic_loader.asyncload.ImageLoadInput;
import com.example.alex.generic_loader.asyncload.Loadable;
import com.example.alex.generic_loader.asyncload.Modelable;
import com.example.alex.generic_loader.asyncload.Viewable;
import com.example.alex.generic_loader.asyncload.WorkerTask;
import com.example.alex.generic_loader.asyncload.impl.AsyncBitmapDrawable;
import com.example.alex.generic_loader.asyncload.impl.EmojiSpannable;
import com.example.alex.generic_loader.util.BitmapDecoder;

/**
 * Created by alex on 6/9/2015.
 */
public class AsyncTextView extends TextView {

    private SpannableViewable spannableViewable;
    private BitmapViewable bitmapViewable;

    public AsyncTextView(Context context) {
        super(context);
        init();
    }

    public AsyncTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AsyncTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        spannableViewable = new SpannableViewable();
        bitmapViewable = new BitmapViewable();
    }

    public BitmapViewable getBitmapViewable() {
        return bitmapViewable;
    }

    public SpannableViewable getSpannableViewable() {
        return spannableViewable;
    }

    private class SpannableViewable implements Viewable<String, String, Spannable> {

        private EmojiSpannable spannable;

        @Override
        public void update(Spannable value) {
            setText(value);
            spannable = null;
        }


        @Override
        public Loadable<String, String, Spannable> getLoadable() {
            return new Loadable<String, String, Spannable>() {
                //Dummy emoji interpretation
                public final String[] emojis = new String[]{":-)", ":-(", ";-)", ":-P",
                        "=-O", ":-*", ":O"};
                public final int[] images = new int[]{R.drawable.simley_10_256, R.drawable.simley_12_24,
                        R.drawable.smiley_10_64, R.drawable.smiley_6_32, R.drawable.smiley_6_64,
                        R.drawable.smiley_8_256, R.drawable.smiley_9_48};


                @Override
                public Spannable loadData(String input, String key) {
                    //Dummy delay to show the actual loading in the ui
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    SpannableStringBuilder spannable = new SpannableStringBuilder(input);

                    for (int i = 0; i < emojis.length; i++) {
                        int lastSpanMarker = 0;
                        while (true) {
                            String emoji = emojis[i];
                            int start = input.indexOf(emoji, lastSpanMarker);
                            if (start >= 0) {
                                Drawable drawable = getResources().getDrawable(images[i]);
                                drawable.setBounds(0, 0, 200, 200);
                                lastSpanMarker = start + emoji.length();
                                spannable.setSpan(new ImageSpan(drawable), start, lastSpanMarker, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            } else {
                                break;
                            }
                        }
                    }

                    return spannable;
                }
            };


        }

        @Override
        public void createModelable(WorkerTask<String, String, Spannable> task) {
            spannable = new EmojiSpannable("Place holder until loading", getResources(), task);
            setText(spannable);
        }

        @Override
        public Modelable<String, String, Spannable> getModelable() {
            return spannable;
        }

    }

    private class BitmapViewable implements Viewable<ImageLoadInput, Integer, BitmapDrawable> {

        private AsyncBitmapDrawable asyncBitmapDrawable;

        @Override
        public void update(BitmapDrawable value) {
            setCompoundDrawables(null, value, null, null);
        }


        @Override
        public Loadable<ImageLoadInput, Integer, BitmapDrawable> getLoadable() {
            return new Loadable<ImageLoadInput, Integer, BitmapDrawable>() {
                @Override
                public BitmapDrawable loadData(ImageLoadInput input, Integer key) {
                    //Dummy delay to show the actual loading in the ui
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    Bitmap bitmap = BitmapDecoder.decodeBitmap(getResources(), key, input.getWidth(), input.getHeight());
                    if (bitmap != null) {
                        BitmapDrawable result = new BitmapDrawable(getResources(), bitmap);
                        result.setBounds(0, 0, input.getWidth(), input.getHeight());
                        return result;
                    }
                    return null;
                }

            };


        }

        @Override
        public void createModelable(WorkerTask<ImageLoadInput, Integer, BitmapDrawable> task) {
            asyncBitmapDrawable = new AsyncBitmapDrawable(getResources(), null, task);
            setCompoundDrawables(null, asyncBitmapDrawable, null, null);
        }

        @Override
        public Modelable<ImageLoadInput, Integer, BitmapDrawable> getModelable() {
            return asyncBitmapDrawable;
        }
    }
}
