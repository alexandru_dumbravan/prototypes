package com.example.alex.generic_loader.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.alex.generic_loader.R;
import com.example.alex.generic_loader.asyncload.ImageLoadInput;
import com.example.alex.generic_loader.asyncload.LoadManager;
import com.example.alex.generic_loader.bean.ListItem;
import com.example.alex.generic_loader.views.AsyncImageView;
import com.example.alex.generic_loader.views.AsyncTextView;

import java.util.List;

/**
 * Created by alex on 6/11/2015.
 */
public class ListAdapter extends ArrayAdapter<ListItem> {

    private LoadManager<String, String, Spannable> emojiLoader = new LoadManager<String, String, Spannable>();
    private LoadManager<ImageLoadInput, Integer, Bitmap> bimapLoader = new LoadManager<ImageLoadInput, Integer, Bitmap>();
    private LoadManager<ImageLoadInput, Integer, BitmapDrawable> drawableLoader = new LoadManager<ImageLoadInput, Integer, BitmapDrawable>();
    private ImageLoadInput imageInput = new ImageLoadInput(200, 200);

    public ListAdapter(Context context, List<ListItem> objects) {
        super(context, R.layout.list_item, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ListItem item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item, parent, false);
            viewHolder.text = (AsyncTextView) convertView.findViewById(R.id.text);
            viewHolder.image = (AsyncImageView) convertView.findViewById(R.id.image);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        emojiLoader.loadDataOnParallelExecutor(viewHolder.text.getSpannableViewable(), item.getText(), item.getText());
        bimapLoader.loadDataOnParallelExecutor(viewHolder.image, item.getResourceId(), imageInput);
        drawableLoader.loadDataOnParallelExecutor(viewHolder.text.getBitmapViewable(), item.getResourceId(), imageInput);
        // Return the completed view to render on screen
        return convertView;
    }

    private static class ViewHolder {
        AsyncTextView text;
        AsyncImageView image;
    }
}
