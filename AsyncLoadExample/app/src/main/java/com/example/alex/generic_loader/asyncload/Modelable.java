package com.example.alex.generic_loader.asyncload;

/**
 * Created by alex on 6/16/2015.
 */
public interface Modelable<I, K extends Comparable<K>, V> {

    /**
     * Called to verify if the input for the worker task is the same as the one running in order not to set a different result on a Viewable
     *
     * @return the worker task associated with this Loadable. Should be the one set in #setWorkerTask
     */
    WorkerTask<I, K, V> getWorkerTask();
}
