package com.example.alex.generic_loader.bean;

/**
 * Created by alex on 6/11/2015.
 */
public class ListItem {

    private int resourceId;
    private String text;

    public ListItem(int resourceId, String text) {
        this.resourceId = resourceId;
        this.text = text;
    }

    public int getResourceId() {
        return resourceId;
    }

    public String getText() {
        return text;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public void setText(String text) {
        this.text = text;
    }
}
