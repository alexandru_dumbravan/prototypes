package com.example.alex.generic_loader.asyncload.impl;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import com.example.alex.generic_loader.asyncload.ImageLoadInput;
import com.example.alex.generic_loader.asyncload.Modelable;
import com.example.alex.generic_loader.asyncload.WorkerTask;

import java.lang.ref.WeakReference;

/**
 * Created by alex on 6/15/2015.
 */
public class AsyncBitmapDrawable extends BitmapDrawable implements Modelable<ImageLoadInput, Integer, BitmapDrawable> {

    private WeakReference<WorkerTask<ImageLoadInput, Integer, BitmapDrawable>> workerTask;

    public AsyncBitmapDrawable(Resources res, Bitmap bitmap, WorkerTask<ImageLoadInput, Integer, BitmapDrawable> task) {
        super(res, bitmap);
        this.workerTask = new WeakReference<WorkerTask<ImageLoadInput, Integer, BitmapDrawable>>(task);
    }

    @Override
    public WorkerTask<ImageLoadInput, Integer, BitmapDrawable> getWorkerTask() {
        return workerTask.get();
    }


}