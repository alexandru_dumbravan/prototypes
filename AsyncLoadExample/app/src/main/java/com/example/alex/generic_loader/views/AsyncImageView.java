package com.example.alex.generic_loader.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.example.alex.generic_loader.asyncload.ImageLoadInput;
import com.example.alex.generic_loader.asyncload.Loadable;
import com.example.alex.generic_loader.asyncload.Modelable;
import com.example.alex.generic_loader.asyncload.Viewable;
import com.example.alex.generic_loader.asyncload.WorkerTask;
import com.example.alex.generic_loader.asyncload.impl.AsyncDrawable;
import com.example.alex.generic_loader.util.BitmapDecoder;

/**
 * Created by alex on 6/10/2015.
 */
public class AsyncImageView extends ImageView implements Viewable<ImageLoadInput, Integer, Bitmap> {

    private AsyncDrawable asyncDrawable;

    public AsyncImageView(Context context) {
        super(context);
    }

    public AsyncImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AsyncImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    public void update(Bitmap value) {
        setImageBitmap(value);
    }


    @Override
    public Loadable<ImageLoadInput, Integer, Bitmap> getLoadable() {
        return new Loadable<ImageLoadInput, Integer, Bitmap>() {
            @Override
            public Bitmap loadData(ImageLoadInput input, Integer key) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return BitmapDecoder.decodeBitmap(getResources(), key, input.getWidth(), input.getHeight());
            }
        };
    }

    @Override
    public void createModelable(WorkerTask<ImageLoadInput, Integer, Bitmap> task) {
        asyncDrawable = new AsyncDrawable(getResources(), null, task);
        setImageDrawable(asyncDrawable);
    }

    @Override
    public Modelable<ImageLoadInput, Integer, Bitmap> getModelable() {
        return asyncDrawable;
    }


}
