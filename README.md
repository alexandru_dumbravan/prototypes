This repository contains a set of prototypes for different Android components:

1. AsyncLoadExample: represents a generalization of loading bitmaps efficiently from [here](http://developer.android.com/training/displaying-bitmaps/index.html). The generalization applies for multiple types of data, for different types of views and for different ways of loading the data. All by using the same loading logic as in the tutorial. In this project 3 types of loading are performed:
a) Bitmaps on ImageViews
b) BitmapDrawables on TextViews
c) Text with embedded images on TextViews

2. ResultReceiverExample: represents a generalization of communication between the UI level and Service level with the use of ResultReceivers. The project contains a ResultReceiver abstraction which can receive either a "Success" message or an "Error" message from the Service and the associated data with each. It also contains a WeakReference towards the component using the ResultReceiver to prevent possible leaks. In the project a MockService is used that gives a certain output based on the provided info and 2 results receivers are used (1 for an activity and 1 for a fragment) to post the updates on the UI. This represents a delegation-based solution with no broadcasts and no global events at the application level.